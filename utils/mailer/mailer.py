from genericpath import exists
import os
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags

""" Funcion para enviar correos mediante SMTP
    Recibe un dict con las propiedades:
    asunto: Asunto del correo
    template: Template html a renderizar
    - Primero se debe de crear el template sobre la carpeta utils/mailer/templates
    template_ctx: Datos que se van a renderizar en el template
    correo_usuario: Correo del usuario al que se va a enviar el correo
"""
def enviar_correo(data):
    """ TODO: añadir validaciones """
    asunto = data['asunto']
    template = data['template']
    template_ctx = data['template_ctx']
    correo_usuario = data['correo_usuario']
    html_message = render_to_string(template, template_ctx)
    plain_message = strip_tags(html_message)
    if 'from' in data:
        from_email = data['from']
    else:
        from_email = None
    send_mail(
        subject = asunto,
        message = plain_message,
        from_email = from_email,
        recipient_list= [correo_usuario],
        html_message = html_message
    )
    