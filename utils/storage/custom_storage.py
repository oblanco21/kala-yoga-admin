from storages.backends.azure_storage import AzureStorage
from django.conf import settings

# Para mas informacion consultar: https://django-storages.readthedocs.io/en/latest/backends/azure.html
# tambien: https://medium.com/@DawlysD/django-using-azure-blob-storage-to-handle-static-media-assets-from-scratch-90cbbc7d56be

class AzureMediaStorage(AzureStorage):
    account_name = settings.AZURE_ACCOUNT_NAME 
    account_key = settings.AZURE_BLOB_STORAGE_ACCESS_KEY
    azure_container = settings.MEDIA_LOCATION
    expiration_secs = None