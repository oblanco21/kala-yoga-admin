# Sistema de administracion Kala Yoga
Nombre del proyecto: Proyecto Aplicación Web Administrativa y Gestión del estudio Kala Yoga
***
Integrantes: 
***
 * José Alejandro Ruíz Suárez
 * Jesús Osvaldo Blanco Vega
 * Luis Fernando Rojas Sánchez
 * Keylin Valeria Castillo Romero
***
Descripcion del proyecto: El sistema web de administracion y gestion contará con dos apartados importantes, uno con la vista del usuario donde el usuario podrá realizar reservaciones de los diferentes servicios como clases, talleres y retiros, así mismo realizar el pago de estos servicios y productos de la tienda virtual. Y el segundo apartado con la vista del administrador que podrá administrar los servicios, crear horarios, vista de las reservaciones realizadas, y contará con un inventario de los productos de la tienda virtual. 


## Requisitos posteriores
* [Instalar python v3.10.4 y pip](https://www.python.org/ftp/python/3.10.4/python-3.10.4-amd64.exe)
* Instalar [virtualenv](https://virtualenv.pypa.io/en/latest/installation.html#via-pip) para crear un entorno virtual que va ser utilizado por el app 
```
// Comando de instalacion virtualenv
pip install virtualenv

// Comando de creacion entorno virtual
virtualenv <nombre_entorno>

// Comando activacion entorno virtual
<nombre_entorno>/Scripts/activate
```
## Desarrollo
* Primero debemos instalar las dependencias que se encuentran en el archivo requeriments.txt => `pip install -r requirements.txt`
* **Siempre que se añadan nuevas dependencias correr el comando** `pip freeze > requirements.txt`
* Crear un archivo `.env` para almacenar los datos de conexion a la base de datos con las siguientes variables:
```
DB_NAME=<VALOR>
DB_HOST=<VALOR>
DB_USER=<VALOR>
DB_PASSWORD=<VALOR>
```
* Correr el comando `python manage.py migrate` para actualizar la base de datos con los ultimos cambios aplicados.

* Para correr el proyecto de manera local ejecutar el comando `python manage.py runserver` esto creara un servidor en el puerto 8000 => localhost:8000

## Variables de entorno requeridas
### Mailer
Para pruebas locales se puede crear una cuenta en [mailtrap.io](https://mailtrap.io/) para simular un servidor SMTP y añadir las siguientes variables al archivo `.env`
```
EMAIL_HOST = <SMTP_HOST>
EMAIL_HOST_USER = <SMTP_USER>
EMAIL_HOST_PASSWORD = <SMTP_PASSWORD>
EMAIL_PORT = <SMTP_PORT>
FROM_EMAIL = <MAILTRAP_IO_EMAIL>
```

### Azure Blob Storage
Consultar el siguiente enlace para ver como [obtener los datos de acceso](https://docs.microsoft.com/en-us/azure/storage/blobs/storage-quickstart-blobs-python?tabs=environment-variable-windows)
```
AZURE_BLOB_STORAGE_ACCESS_KEY=<ACCESS_KEY>
AZURE_BLOB_STORAGE_CONNECTION_STRING=<CONNECTION_STRING>
```

### Postmark: SMTP service
Para que las funciones de correo electronico funcionen correctamente, se debe de generar un token en [Postmark](https://postmarkapp.com/) y añadirlo al .env:

```
POSTMARK_TOKEN=<TOKEN>
```