from django.http import HttpResponse, JsonResponse
from django.shortcuts import redirect, render
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from models.UsuarioInfo import Contactos, Notificaciones, UsuarioInfo
from models.Reservaciones import Eventos, Reservaciones, Tipo_Clase
from utils.mailer.mailer import enviar_correo
from .filtros import buscarClientes
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.core import serializers
from administracion.forms import ProductoForm
from models.models import Orden, Orden_detalle, Producto
from django.db.models import F
import random
from django.contrib.auth.tokens import default_token_generator
from django.db.models import Q
import json
from datetime import datetime,timedelta, time
import datetime as dt
from django.contrib.admin.views.decorators import staff_member_required
import urllib.request
from django.conf import settings
from django.utils.http import urlsafe_base64_encode
from django.contrib.auth.tokens import default_token_generator
from django.utils.encoding import force_bytes
from django.contrib.sites.shortcuts import get_current_site

# ---- Views de inicio y registro de sesion

def login_admin(request):
    if request.method == 'POST':
        email = request.POST['email']
        password = request.POST['password']

        #Configuración de Captcha
        recaptcha_response = request.POST.get('g-recaptcha-response')
        url = 'https://www.google.com/recaptcha/api/siteverify'
        values = {
        'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
        'response': recaptcha_response
        }
        data = urllib.parse.urlencode(values).encode('utf-8')
        req = urllib.request.Request(url, data)
        response = urllib.request.urlopen(req)
        result = json.load(response)

        # Cuando el captcha ha sido completado
        if result['success']:
            try:
                user = User.objects.get(username = email)  
                es_staff = False
                if user.is_staff:
                    es_staff = True

                # Cuando usuario está activo
                if user.is_active :
                    cliente = UsuarioInfo.objects.get(usuario=user.id)
                    user = authenticate(request, username=email, password=password) 
                    # Cuando usuario ingresa correctamente
                    if user is not None:
                        if es_staff:
                            cliente.logins_fallidos = 0
                            cliente.save()
                            login(request, user)
                            return redirect('home')   
                        else:
                            return redirect('login')
                    # Cuando se ingresa contraseña incorrecta
                    else:
                        # Cuando logins_fallidos es menor a 10
                        if cliente.logins_fallidos >= 9:
                            user = User.objects.get(username = email) 
                            user.is_active = 0
                            user.save()
                        # Cuando ya hay 10 logins fallidos
                        else:
                            cliente.logins_fallidos += 1
                            cliente.save()

                        messages.error(request,"Usuario o contraseña incorrecta")

                # Cuando usuario está bloqueado
                else:
                    messages.error(request,'Cuenta bloqueda, prueba "olvidé mi contraseña"')     
            # Cuando usuario no existe o algo en el proceso sale mal  
            except:
                messages.error(request,"No se encuentra este usuario")
        # Cuando el captcha es inválido
        else:
            messages.error(request, 'Para iniciar sesión, valida que no eres un robot!')



        
    return render(request, 'login_admin.html')

def logoutAdmin(request):
    logout(request)
    return redirect('login_admin')

def olvidar_contrasena(request):
    return render(request,'olvidar_contrasena.html')

def recuperar_contrasena(request):
    return render(request, 'recuperar_contrasena.html')

# --- Pruebas

def prueba(request):
    User.objects.create_user(username="a@z.com",password="12345678",email="a@z.com")
    return render(request,'prueba.html')

# --- Views una vez iniciada sesión

# @login_required(login_url='login_admin')
@staff_member_required(login_url='login_admin')
def home(request):

    #graficos
    usuarioInfo = UsuarioInfo.objects.all()
    masculino = usuarioInfo.filter(genero='Masculino').count()
    femenino = usuarioInfo.filter(genero='Femenino').count()
    otro = usuarioInfo.filter(genero='Otro').count()
    productos = Producto.objects.all()
    coloresGenero = []
    coloresProductos = []

    for x in range(3):
        r = str(random.randint(0,255))
        g = str(random.randint(0,255))
        b = str(random.randint(0,255))
        a = str(1)
        rgba = 'rgba(' + r + ',' + g + ',' + b + ',' + a + ')'
        coloresGenero.append(rgba)

    for x in productos:
        r = str(random.randint(0,255))
        g = str(random.randint(0,255))
        b = str(random.randint(0,255))
        a = str(1)
        rgba = 'rgba(' + r + ',' + g + ',' + b + ',' + a + ')'
        coloresProductos.append(rgba)

    context = {'Masculino': masculino, 'Femenino': femenino, "Otro": otro, "Productos": productos, "ColoresGenero": coloresGenero,
    "ColoresProducto": coloresProductos}

    #notificacion de producto bajjo de stock

    productos = Producto.objects.filter(cantidad=3)
    
    # for a in productos:
    #     # Crear Notificación

    #     notificacion = Notificaciones.objects.create(
    #         visto = 0,
    #         mensaje = 'Solo quedan ' + str(a.cantidad) + '  de ' + a.nombre + ' en stock'
    #     )

    #     notificacion.save()
       


    return render(request, 'home.html', context)


# --- Read y Filtro de Clientes + Paginacion ---
@staff_member_required(login_url='login_admin')
def clientes(request):
    checkMensualidades()
    clienteObj, buscarCliente = buscarClientes(request)

    page = request.GET.get('page')
    results = 6  # Considerar solucion cuando hay demasiadas paginas
    paginator = Paginator(clienteObj, results)
    try:
        clienteObj = paginator.page(page)
    except PageNotAnInteger:
        page = 1
        clienteObj = paginator.page(page)
    except EmptyPage:
        page = paginator.num_pages
        clienteObj = paginator.page(page)

    context = {'clienteObj': clienteObj,
               'buscarCliente': buscarCliente, 'paginator': paginator}
    return render(request, 'clientes.html', context)


@staff_member_required(login_url='login_admin')
def productos(request):
    action = request.GET.get('action', 'none')
    if action == 'create':
        return post_productos(request)
    elif action == 'edit':
        pk = request.GET.get('id')
        return put_productos(request, pk)
    elif action == 'delete':
        pk = request.GET.get('id')
        return delete_productos(request, pk)
    else:
        productos = Producto.objects.all()
        ordenes_confirmadas = Orden.objects.filter(estado="CONFIRMADO").count()
        return render(request, 'productos/main.html', {'productos': productos, 'total_ordenes': ordenes_confirmadas, 'action': 'list'})


@staff_member_required(login_url='login_admin')
def post_productos(request):
    form = ProductoForm()
    if request.method == 'POST':
        form = ProductoForm(request.POST, request.FILES)

        if form.is_valid():
            imgFiles = request.FILES['imagen']
            form.save()
            messages.success(request, 'El producto ha sido creado con exito')
        else:
            print(form.errors)
            messages.error(
                request, 'Ha ocurrido un error al registrar el producto')

        return redirect('productos')
    return render(request, 'productos/main.html', {'action': 'create', 'form': form})

@staff_member_required(login_url='login_admin')
def delete_productos(request, pk):
    prod = Producto.objects.get(id=pk)
    prod.delete()
    messages.success(request, 'Producto eliminado con éxito')
    return redirect('productos')


@staff_member_required(login_url='login_admin')
def put_productos(request, pk):
    producto = Producto.objects.get(pk=pk)
    form = ProductoForm(instance=producto)
    productoObj = {
        'id': producto.id,
        'nombre': producto.nombre,
        'precio': producto.precio,
        'cantidad': producto.cantidad,
        'imagen': producto.imagen
    }
    print(productoObj)
    if request.method == 'POST':
        req = request.POST
        imagen = request.FILES.get('imagen', False)
        producto.nombre = req['nombre']
        producto.precio = req['precio']
        producto.cantidad = req['cantidad']
        if(imagen):
            producto.imagen = imagen

        producto.save()
        messages.success(request, 'Producto editado correctamente')
        return redirect('productos')
    return render(request, 'productos/main.html', {'action': 'edit', 'form': form, 'data': productoObj})


@staff_member_required(login_url='login_admin')
def post_clientes(request):

    if request.method == 'POST':
        formData = request.POST
        
        """ Validar que el correo no esta registrado """
        try:
            user = User.objects.get(email = formData['email'])
            messages.info(request, 'El correo electronico ya se encuentra registrado')
            return render(request, 'registrar_usuario.html')
        except User.DoesNotExist:
            pass

        user = User.objects.create(
            username=formData['email'], 
            email=formData['email']
        )
        
        user.last_name = formData['apellidos']
        user.first_name = formData['nombre']
        user.set_password('validacion')
        
        user.save()

        UsuarioInfo.objects.create(
            usuario=user,
            genero=formData['genero'],
            cedula=formData['cedula'],
            edad=formData['edad'],
            lesiones=formData['lesiones'],
            padecimientos=formData['padecimientos']
        )

        messages.success(request, 'Usuario creado con éxito')
        send_reset_password_request(user)
        return redirect('clientes')

    return render(request, 'registrar_usuario.html')

def send_reset_password_request(request, user):
    print(f'Creating reset password request parameters for user => {user.email}')
    userUID = urlsafe_base64_encode(force_bytes(user.pk))
    token = default_token_generator.make_token(user)

    current_site = get_current_site(request)
    domain = current_site.domain
    
    email_data = {
        'asunto': 'Bienvenido a Kala Yoga Online',
        'template': 'recuperar_contrasena_admin.html',
        'template_ctx': {
            'protocol': 'http',
            'domain': domain,
            'uid': userUID,
            'token': token,
            'user': user.email
        },
        'correo_usuario': user.email
    }

    enviar_correo(email_data)

@staff_member_required(login_url='login_admin')
def registrarr(request):
    if request.method == 'POST':
        formData = request.POST
        
        """ Validar que el correo no esta registrado """
        try:
            user = User.objects.get(email = formData['email'])
            messages.info(request, 'El correo electronico ya se encuentra registrado')
            return render(request, 'registrar_usuario.html')
        except User.DoesNotExist:
            pass

        user = User.objects.create(
            username=formData['email'], 
            email=formData['email']
        )
        
        user.last_name = formData['apellidos']
        user.first_name = formData['nombre']
        user.set_password('validacion')
        
        user.save()

        UsuarioInfo.objects.create(
            usuario=user,
            genero=formData['genero'],
            celular=formData['celular'],
            cedula=formData['cedula'],
            edad=formData['edad'],
            lesiones=formData['lesiones'],
            padecimientos=formData['padecimientos']
        )

        messages.success(request, 'Usuario creado con éxito')
        send_reset_password_request(request, user)
        return redirect('clientes')

    return render(request,'registrar_usuario.html')


@staff_member_required(login_url='login_admin')
def calendario(request):

# Tarea para crear eventos siguientes
    tipoClase = Tipo_Clase.objects.all()
    today = dt.date.today()
    print(today + timedelta(8))
    for a in tipoClase:
        fechas = siguientesFechas(dt.date.today(),a.dia)
        for x in fechas:
            try:
                eventoXreservar = Eventos.objects.get(tipoClase=a.id,fecha=x)
                # print('Ya existe evento id: '+str(a.id)+' en la fecha: '+str(x))

            except Eventos.DoesNotExist:
                eventoXcrear = Eventos.objects.create(
                    fecha = x,
                    tipoClase_id = a.id,
                    estado = True
                )
                eventoXcrear.save()
                # print('NO existe evento id: '+str(a.id)+' en la fecha: '+str(x))

# Traer las clases para rellenar el calendario
    eventos = Eventos.objects.select_related('tipoClase').all()
    # eventosObj = serializers.serialize("json", eventosObj)
    usuarios = User.objects.all().filter(is_staff=False).order_by('first_name')
    tipoClases = Tipo_Clase.objects.all().order_by('nombre')

    listEvents = []

    for evento in eventos:
        listEvents.append({
            'nombre_clase': evento.tipoClase.nombre,
            'fecha': str(evento.fecha),
            'id': evento.id, # usar este valor para obtener los usuarios que han realizado reservaciones
            'horario_inicio': evento.tipoClase.horario_inicio.strftime("%H %M"),
            'horario_cierre': evento.tipoClase.horario_cierre.strftime("%H %M"),
            'aforo': evento.tipoClase.aforo
        })
    
    listEvents = json.dumps(listEvents)
    context = {'eventosObj': listEvents,'users':usuarios,'tipoClases':tipoClases}
    return render(request, 'calendario.html', context)

# --- Editar Cliente


@staff_member_required(login_url='login_admin')
def editar(request, pk):
    #Se obtienen los objetos que tengan el mismo id que se le envia a la función
    userInfoObj = UsuarioInfo.objects.get(usuario_id=pk)
    userObj = User.objects.get(id=pk)
    #Si se presiona el botón editar entra a este if
    if request.method == 'POST':
        #se reemplazan los valores actuales con los que el usuario escribió
        userObj.first_name = request.POST['first_name']
        userObj.last_name = request.POST['last_name']
        userObj.email = request.POST['email']
        
        userInfoObj.cedula = request.POST['cedula']
        userInfoObj.celular = request.POST['celular']
        userInfoObj.edad = request.POST['edad']
        userInfoObj.genero = request.POST['genero']
        userInfoObj.lesiones = request.POST['lesiones']
        userInfoObj.padecimientos = request.POST['padecimientos']
        #se guardan los valores
        User.save(userObj)
        UsuarioInfo.save(userInfoObj)
        #se envía el mensaje de éxito
        messages.success(request, 'Usuario editado con éxito')
        #se redirige a la pagina de clientes
        return redirect('clientes')
    #se renderiza la página con los datos que coinciden con el ID que se envió
    return render(request,'Editar.html', {'userObj': userObj, 'userInfoObj': userInfoObj})

@staff_member_required(login_url='login_admin')
def mensualidad(request,pk):
    userInfoObj = UsuarioInfo.objects.get(usuario_id=pk)
    userObj = User.objects.get(id=pk)

    if request.method == 'POST':
        userInfoObj.tipo_mensualidad = request.POST['tipo_mensualidad']
        userInfoObj.mensualidad_aldia = request.POST['mensualidad_aldia']
        userInfoObj.ultima_mensualidad = request.POST['ultima_mensualidad']
        userInfoObj.save()

        return redirect('clientes')

    return render(request,'mensualidad.html', {'userObj': userObj, 'userInfoObj': userInfoObj})

# --- Eliminar cliente

@staff_member_required(login_url='login_admin')
def deleteuser(request, pk):
    #Se obtienen los objetos que tengan el mismo id que se le envia a la función
    user = User.objects.get(id = pk)
    userInfo = UsuarioInfo.objects.get(usuario_id=pk)
    #Se elimina el objeto
    user.delete()
    userInfo.delete()
    #Se envía el mensaje de que se eliminó con éxito
    messages.success(request, 'Usuario eliminado con éxito')
    #se redirige a la página de clientes
    return redirect('clientes')


@staff_member_required(login_url='login_admin')
def get_evento(request, pk):
    print(request)
    evento = Eventos.objects.get(pk=pk)
    evento = serializers.serialize('json', [evento])
    return HttpResponse(evento, content_type='application/json')

@staff_member_required(login_url='login_admin')
def get_evento_reservas(request, pk):
    reservaciones = Reservaciones.objects.filter(evento_id=pk)
    usersArray = []
    for u in reservaciones:
        user = User.objects.get(id=u.user_id)
        usersArray.append({'id': user.id, 'nombre': user.first_name, 'apellidos': user.last_name})

    usersArray = json.dumps(usersArray)
    return HttpResponse(usersArray, content_type='application/json')

@staff_member_required(login_url='login_admin')
def delete_evento_reservas(request, pk, reserva):
    
    reserva = Reservaciones.objects.get(user_id=reserva, evento_id=pk)
    
    try:
        reserva.delete()
        response = 200
    except Exception:
        response = 400

    return HttpResponse(response, content_type='application/json')

@staff_member_required(login_url='login_admin')
def verHorario(request):
    tipo_clases = Tipo_Clase.objects.all()
    return render(request, 'horario.html', {'tipoClases': tipo_clases})

@staff_member_required(login_url='login_admin')
def get_evento_horario_details(request, pk):
    evento = Tipo_Clase.objects.get(pk=pk)
    evento = serializers.serialize('json', [evento])
    return HttpResponse(evento, content_type='application/json')

@staff_member_required(login_url='login_admin')
def put_evento_horario_details(request, pk):
    evento = Tipo_Clase.objects.get(pk=pk)

    if request.method == 'POST':
        body = json.loads(request.body)
        formData = body['data']
        inicio = datetime.strptime(formData['horario_inicio'], "%H:%M").time()
        cierre = datetime.strptime(formData['horario_cierre'], "%H:%M").time()

        evento.nombre = formData['nombre']
        evento.precio = formData['precio']
        evento.horario_inicio = inicio
        evento.horario_cierre = cierre
        evento.dia = formData['dia']
        evento.descripcion = formData['descripcion']

        evento.save()
        return HttpResponse(json.dumps({ 'msg': 'Evento actualizado', 'action':'redirect', 'code': 200}), content_type='application/json')

def delete_evento_horario(request, pk):
    pass

@staff_member_required(login_url='login_admin')
def postEventoHorario(request):
    if request.method == 'POST':
        formData = request.POST
        
        inicio = datetime.strptime(formData['horario_inicio'], "%H:%M").time()
        cierre = datetime.strptime(formData['horario_cierre'], "%H:%M").time()

        try:
            print('Inside try/exception')
            evento = Tipo_Clase.objects.create(
                nombre=formData['nombre'],
                precio=formData['precio'],
                descripcion=formData['descripcion'],
                horario_inicio=inicio,
                horario_cierre=cierre,
                dia=formData['dia-evento'],
                aforo=formData['aforo']
            )

            evento.save()

            messages.success(request, 'Evento añadido correctamente')

        except Exception:
            messages.error(request, Exception)
    return redirect('horario')

#Get todas las notificaciones 
# @staff_member_required(login_url='login_admin')
# def get_notifs(request):
#     data = Notificaciones.objects.filter(visto=False)
#     jsonData = serializers.serialize('json',data)
#     cuenta = Notificaciones.objects.filter(visto=False).count()
#     return JsonResponse({'data':jsonData, 'cuenta':cuenta})

# @staff_member_required(login_url='login_admin')
# def post_notifs(request):

    if request.method=='POST':
        print(request)
        notif = Notificaciones.objects.get(id = request.POST['id'])
        
        notif.visto = True
        notif.save()

        return JsonResponse({'mensaje': 'success'})

    return JsonResponse({'mensaje': 'failed'})

@staff_member_required(login_url='login_admin')
def traerDatosClase(request,pk):
    try:
        tipo_clase = Tipo_Clase.objects.get(id=pk)
        data = {
            'dia':tipo_clase.dia,
            'hora': tipo_clase.horario_inicio
        }
    except Tipo_Clase.DoesNotExist:
        pass
    return JsonResponse(data)

@staff_member_required(login_url='login_admin')
def crearReserva(request):

    if request.method=='POST':
        formData = request.POST
        id_tipo_clase = formData['id_tipo_clase']
        id_tipo_clase = id_tipo_clase.split('-')
        id_tipo_clase = id_tipo_clase[0]


        try:
            # Caso de que evento ya existe

            eventoxreservar = Eventos.objects.get(fecha=formData['fecha'],tipoClase_id=id_tipo_clase)

            # Crear reserva
            reserva = Reservaciones.objects.create(
                evento_id = eventoxreservar.id,
                user_id = formData['id_cliente']
            )    
            reserva.save()


        except Eventos.DoesNotExist:
            evento = Eventos.objects.create(
                estado=True,
                fecha=formData['fecha'],
                tipoClase_id=id_tipo_clase
            )
            evento.save()
            print('evento creado')

            eventoxreservar = Eventos.objects.latest('id')
            print('id de evento x reservar: ',eventoxreservar.id)
            
            reserva = Reservaciones.objects.create(
                evento_id = eventoxreservar.id,
                user_id = formData['id_cliente']
            )    
            reserva.save()


        # Crear Notificación

        clase = Tipo_Clase.objects.get(id=eventoxreservar.tipoClase_id)
        claseNombre = clase.nombre
        claseHora = clase.horario_inicio
        notificacion = Notificaciones.objects.create(
            visto = 0,
            mensaje = 'Admin creó una reservación para ' + claseNombre + ' el ' + formData['fecha']
        )

        notificacion.save()
        return redirect('calendario')

# @staff_member_required(login_url='login_admin')
# def listarNotificaciones(request):
#     notificaciones = Notificaciones.objects.all().order_by('visto')

#     return render(request,'notificaciones.html',{'notificaciones': notificaciones})

@staff_member_required(login_url='login_admin')
def mensajes(request):
    contactos = Contactos.objects.all()[:10]
    return render(request,'mensajes.html',{'contactos': contactos})

@staff_member_required(login_url='login_admin')
def get_ordenes(request):
    ordenes_confirmadas = Orden.objects.filter(estado="CONFIRMADO")
    return render(request, 'ordenes.html', { 'ordenes': ordenes_confirmadas })

@staff_member_required(login_url='login_admin')
def get_orden_detalle(request, orden):
    orden = Orden.objects.get(id=orden)
    detalle = Orden_detalle.objects.filter(orden=orden)
    return render(request, 'orden-detalle.html', { 'detalles': detalle, 'total_orden': orden.total_orden })

def siguientesFechas(d, letradia):
    fechasSiguientes = []

    if letradia == 'L':
        weekday = 0
    elif letradia == 'K':
        weekday = 1
    elif letradia == 'M':
        weekday = 2
    elif letradia == 'J':
        weekday = 3
    elif letradia == 'V':
        weekday = 4
    else :
        weekday = 5

    diaHoy = d.weekday()
    if diaHoy == weekday:
       fechasSiguientes.append(dt.date.today())
        
    days_ahead = weekday - d.weekday()
    if days_ahead <= 0: # Target day already happened this week
        days_ahead += 7
    fechasSiguientes.append(d + timedelta(days_ahead))
    return fechasSiguientes

def checkMensualidades():
    userInfo = UsuarioInfo.objects.filter(mensualidad_aldia = 'al dia')
    today = dt.date.today()
    for a in userInfo:
        # Aqui se consiguen los dias restantes entre fecha actual y ultima mensualidad
        diasrestantes = today - a.ultima_mensualidad
        diasrestantes = str(diasrestantes)
        diasrestantes = diasrestantes.split(' ')
        if diasrestantes[0] == '0:00:00':
            diasrestantes = '0'
        else:
            diasrestantes = diasrestantes[0]
        diasDeultimoPago = int(diasrestantes)
        
        # Se modifican lo estados de la mensualidades a atrasado si llevan mas de 30 dias 

        if diasDeultimoPago >= 31 and diasDeultimoPago < 62:
            a.mensualidad_aldia = 'atrasado'
            a.save()
        elif diasDeultimoPago >= 62:
            a.mensualidad_aldia = 'cancelada'
            a.save()
    
def guia(request):
    return render(request,'guia.html')