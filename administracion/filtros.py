from models.UsuarioInfo import UsuarioInfo
from django.contrib.auth.models import User
from django.db.models import Q

def buscarClientes(request):
    buscarCliente = ''

    if request.GET.get('buscarCliente'):
        buscarCliente = request.GET.get('buscarCliente')

    nombres = User.objects.filter(
        Q(first_name__icontains=buscarCliente)|
        Q(last_name__icontains=buscarCliente)|
        Q(email__icontains=buscarCliente))

    clienteObj = UsuarioInfo.objects.select_related('usuario').filter(
        Q(padecimientos__icontains=buscarCliente) |
        Q(lesiones__icontains=buscarCliente)|
        Q(usuario__in=nombres)|
        Q(mensualidad_aldia__icontains=buscarCliente))
        

    return clienteObj, buscarCliente