from django import views
from django.urls import path
from . import views

urlpatterns = [
    path('',views.login_admin, name='login_admin'),
    path('logout',views.logoutAdmin, name='logout_admin'),
    
    path('home', views.home, name="home"),
    path('clientes', views.clientes, name="clientes"),
    path('registrar', views.registrarr, name="registrar"),
    path('olvidar_contrasena',views.olvidar_contrasena, name="olvidar_contrasena"),
    path('recuperar_contrasena',views.recuperar_contrasena, name="recuperar_contrasena"),

    path('productos',views.productos, name="productos"),
    path('productos/crear',views.post_productos, name="productos-crear"),
    path('productos/editar/<str:pk>',views.put_productos, name="productos-editar"),
    path('ordenes',views.get_ordenes, name="ordenes"),
    path('ordenes/<int:orden>/detalle',views.get_orden_detalle, name="orden-detalle"),

    path('clases',views.calendario,name="calendario"),
    path('reservar',views.crearReserva, name="reservar"),
    path('traer_datos_clase/<int:pk>',views.traerDatosClase, name="traerDatosClase"),
    path('guia',views.guia, name="guia"),

    path('editar/<str:pk>/',views.editar,name="editar"),
    path('mensualidad/<str:pk>/',views.mensualidad,name="mensualidad"),
    path('prueba',views.prueba,name="prueba"),
    path('eliminar/<str:pk>/',views.deleteuser,name="eliminar-usuario"),
    path('eventos/<int:pk>',views.get_evento,name="test-data"),
    path('eventos/<int:pk>/reservas', views.get_evento_reservas, name="evento-reservas"),
    path('eventos/<int:pk>/reservas/<int:reserva>/delete', views.delete_evento_reservas, name="evento-reservas-delete"),

    path('horario',views.verHorario, name="horario"),
    path('horario/evento/create',views.postEventoHorario, name="evento-add"),
    path('horario/evento/<int:pk>',views.get_evento_horario_details, name="evento-get"),
    path('horario/evento/<int:pk>/edit',views.put_evento_horario_details, name="evento-put"),

    # path('get_notifs',views.get_notifs,name='get_notifs'),
    # path('post_notifs',views.post_notifs,name='post_notifs'),
    # path('notificaciones',views.listarNotificaciones,name='notificaciones'),

    path('mensajes',views.mensajes,name='mensajes')
]