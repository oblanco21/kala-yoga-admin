from django import forms
from models.models import Producto
class ProductoForm(forms.ModelForm):
    class Meta:
        imagen = forms.FileField();
        model = Producto
        fields = ['nombre', 'precio', 'cantidad', 'descripcion', 'imagen']
        labels = {
            'nombre': 'Nombre',
            'precio': 'Precio',
            'cantidad': 'Cantidad',
            'descripcion': 'Descripcion',
            'imagen': 'Imagen'
        }

    def __init__(self, *args, **kwargs):
        super(ProductoForm, self).__init__(*args, **kwargs)

        for name, field in self.fields.items():
            field.widget.attrs.update({'class': 'form-control', 'placeholder': field.label})

