from email.policy import default
from random import choices
from django.db import models
from django.contrib.auth.models import User
from .UsuarioInfo import UsuarioInfo
from .Reservaciones import Reservaciones
from django.utils import timezone
import datetime
# Create your models here.

class Producto(models.Model):
    nombre = models.CharField(max_length=100,null=True)
    descripcion = models.CharField(max_length=1000,null=True)
    precio =  models.IntegerField(null=True)
    cantidad = models.IntegerField(null=True)
    imagen = models.ImageField(null=True)

class Orden(models.Model):
    cliente = models.ForeignKey(User, on_delete=models.CASCADE)
    ESTADO_ENUM = [
        ("EN_PROGRESO","en_progreso"),
        ("ABORTADA", "abortada"),
        ("CONFIRMADO", "confirmado"),
        ("ENTREGADO", "entregado")
    ]
    estado = models.CharField(choices=ESTADO_ENUM, default="EN_PROGRESO", max_length=15)
    total_orden = models.IntegerField(default=0)
    fecha_creado = models.CharField(default=datetime.datetime.now, blank=True, max_length=200)
    fecha_confirmado = models.CharField(default=0, null=True, max_length=200)
    fecha_entregado = models.CharField(default=0, null=True, max_length=200)

class Orden_detalle(models.Model):
    orden = models.ForeignKey(Orden, on_delete=models.CASCADE)
    producto = models.ForeignKey(Producto, on_delete=models.CASCADE)
    cantidad = models.IntegerField(default=1)
    precio_unidad = models.IntegerField(default=1)
    
    @property
    def total(self):
        return self.cantidad * self.precio_unidad