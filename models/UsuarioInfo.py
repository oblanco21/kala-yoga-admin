from django.db import models
from django.contrib.auth.models import User

class UsuarioInfo(models.Model):
    usuario = models.ForeignKey(User, on_delete=models.CASCADE)
    edad = models.IntegerField(null=True)
    genero = models.CharField(max_length=45, null=True)
    cedula = models.CharField(max_length=10, null=True)
    lesiones = models.CharField(max_length=100, null=True)
    padecimientos = models.CharField(max_length=100, null=True)
    celular = models.CharField(max_length=11, null=True)
    ultima_mensualidad = models.DateField(null=True)
    mensualidad_aldia = models.CharField(max_length=10,null=False,default='N/A') #al dia - atrasado - N/A
    tipo_mensualidad = models.IntegerField(null=False,default=0)
    logins_fallidos = models.IntegerField(null=False,default=0)

class Notificaciones(models.Model):
    visto = models.BooleanField(default=False)
    mensaje = models.TextField()

class Contactos(models.Model):
    nombre = models.TextField()
    correo = models.TextField()
    celular = models.TextField(null=True)
    mensaje = models.TextField()