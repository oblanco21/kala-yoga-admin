from django.db import models
from django.contrib.auth.models import User

from models.UsuarioInfo import UsuarioInfo

class Tipo_Clase(models.Model):

    nombre = models.CharField(max_length=50,null=False,default='clase')
    descripcion = models.CharField(max_length=2000,null=True)
    horario_inicio = models.TimeField(null=True)
    horario_cierre = models.TimeField(null=True)
    aforo = models.IntegerField(null=False,default=-1)
    dia = models.CharField(null=False, max_length=1)
    precio = models.IntegerField(null=True)

class Eventos(models.Model):

    tipoClase = models.ForeignKey(Tipo_Clase, on_delete=models.CASCADE,null=True)
    fecha = models.DateField(null=True)
    estado = models.BooleanField()


class Reservaciones(models.Model):

    evento = models.ForeignKey(Eventos, on_delete=models.CASCADE)
    user =models.ForeignKey(User, on_delete=models.CASCADE)

