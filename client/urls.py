from django import views
from django.urls import path
from . import views
from django.contrib.auth.urls import views as auth_views

urlpatterns = [
    path('', views.index, name="inicio"),
    path('contacto/', views.contacto, name="contacto"),
    path('tienda/', views.tienda, name="tienda"),
    path('tienda/productos/<str:id>/', views.producto, name="producto"),
    path('login/', views.login_cliente, name="login"),
    path('logout', views.logout_cliente, name="logout_cliente"),
    path('registro/', views.registro, name="registro"),

    path('clases/', views.verClases, name="clases"),
    path('clases/clase_detalle/<int:pk>/', views.verClaseDetalle, name="clase_detalle"),
    path('clases/crear_reserva/<int:id_clase>/', views.crearReserva, name="crear_reserva"),

    path('cuenta/', views.cuenta, name="cuenta"),
    path('cuenta/recuperar', auth_views.PasswordResetView.as_view(template_name='registration/password_reset_email.html', html_email_template_name='recuperar_contrasena.html'), name="password_reset"),
    path('cuenta/recuperar/enviado', auth_views.PasswordResetDoneView.as_view(template_name='registration/password_reset_email.html', extra_context={ 'sent': True }), name="password_reset_done"),
    path('cuenta/recuperar/nueva/<uidb64>/<token>', views.password_reset_confirm_view_custom, name="password_reset_confirm"),
    path('cuenta/recuperar/completado', auth_views.PasswordResetCompleteView.as_view(), name="password_reset_complete"),
    path('cuenta/cancelar_reserva/<int:id_reserva>/', views.cancelarReserva, name="cancelar_reserva"),
    path('carrito/', views.carrito, name="carrito"),  
    path('carrito/orden/<int:orden_id>/confirmar', views.confirmarPedido, name="confirmar_pedido"),  
    path('carrito/orden/<int:orden_id>/detalle', views.getDetalleOrden, name="ver_detalles_orden"),  
    path('carrito/orden/detalle/producto/<int:id_producto>', views.delete_detalle_orden, name="delete-detalle-orden"),  

    #path('checkout/', views.checkout, name = 'checkout'),
    #path('borrar_carrito/', views.borrar_carrito, name ='borrar_carrito'),
    #path('confirmar/', views.confirmar_pedido, name= 'confirmar_pedido'),

    path('nosotros/', views.nosotros, name="nosotros"),
    path('masajes/', views.masajes, name="masajes"),
    path('clasesyoga/', views.clasesyoga, name="clasesyoga"),
    path('retiros/', views.retiros, name="retiros"),
    path('talleres/', views.talleres, name="talleres"),
    path('ayuda/',views.ayuda, name="ayuda"),

    path('prueba_fetch/<str:id>/',views.prueba_fetch,name="prueba_fetch"),
    path('bienvenida/', views.bienvenida, name="bienvenida")

]
