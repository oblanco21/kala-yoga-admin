from ast import Pass
from base64 import urlsafe_b64decode
from cmath import exp
from email import message
from itertools import product
from json import JSONDecoder
from ssl import SSLSession
from time import timezone
from django.shortcuts import HttpResponse, render, redirect
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from models.models import Orden, Orden_detalle, Producto
from administracion.views import clientes, productos
from models.Reservaciones import Reservaciones, Tipo_Clase, Eventos
from models.UsuarioInfo import Contactos, Notificaciones, UsuarioInfo
from models.models import Producto
from utils.mailer.mailer import enviar_correo
import datetime
from datetime import date, timedelta
from django.core import serializers
from django.http import JsonResponse
import json
import urllib.request
from django.conf import settings
from django.utils import timezone
import datetime
from django.contrib.auth.tokens import default_token_generator

# Create your views here.

def index(request):
    if request.user.is_authenticated is False:
        request.session['total_items_orden'] = 0

    return render(request, 'client/home.html')

def login_cliente(request):

    if request.method == 'POST':
        email = request.POST['email']
        password = request.POST['password']

        #Configuración de Captcha
        recaptcha_response = request.POST.get('g-recaptcha-response')
        url = 'https://www.google.com/recaptcha/api/siteverify'
        values = {
        'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
        'response': recaptcha_response
        }
        data = urllib.parse.urlencode(values).encode('utf-8')
        req = urllib.request.Request(url, data)
        response = urllib.request.urlopen(req)
        result = json.load(response)
        
        # Cuando el captcha ha sido completado
        if result['success']:
            try:
                user = User.objects.get(username = email)  

                # Cuando usuario está activo
                if user.is_active :
                    cliente = UsuarioInfo.objects.get(usuario=user.id)
                    user = authenticate(request, username=email, password=password) 
                    # Cuando usuario ingresa correctamente
                    if user is not None:
                        cliente.logins_fallidos = 0
                        cliente.save()
                        login(request, user)
                        getOrdenCount(request, user)
                        return redirect('inicio')   
                    # Cuando se ingresa contraseña incorrecta
                    else:
                        # Cuando logins_fallidos es menor a 10
                        if cliente.logins_fallidos >= 9:
                            user = User.objects.get(username = email) 
                            user.is_active = 0
                            user.save()
                        # Cuando ya hay 10 logins fallidos
                        else:
                            cliente.logins_fallidos += 1
                            cliente.save()

                        messages.error(request,"Usuario o contraseña incorrecta")

                # Cuando usuario está bloqueado
                else:
                    messages.error(request,'Cuenta bloqueda, prueba "olvidé mi contraseña"')     
            # Cuando usuario no existe     
            except:
                messages.error(request,"No se encuentra este usuario")
        # Cuando el captcha es inválido
        else:
            messages.error(request, 'Para iniciar sesión, valida que no eres un robot!')

         

    return render(request, 'client/login.html')

def logout_cliente(request):
    logout(request)
    return redirect('inicio')

def bienvenida(request):
    
    return render(request,'client/bienvenida.html')

def prueba_fetch(request,id):
    try:
        User.objects.get(username=id)
        data = {
            'msj':'Ya existe una cuenta con el correo electrónico ingresado. Prueba otro correo.'
        }
    except User.DoesNotExist:
        data = {
            'msj':'valido'
        }
    return JsonResponse(data)

def registro(request):
    if request.method == 'POST':
        nombre = request.POST['nombre']
        apellido = request.POST['apellido']
        email = request.POST['email']
        password = request.POST['password']

        
        try:

            User.objects.get(username=email)
            messages.error(request,'Ya existe una cuenta con el correo electrónico ingresado. Prueba otro correo.')
                
                

        except User.DoesNotExist:

            print('Creando un nuevo usuario')
            user = User.objects.create_user(username=email,password=password,email=email,
                                         first_name=nombre,last_name=apellido)
                
            userInfo=UsuarioInfo.objects.create(usuario=user,genero=request.POST['genero'],padecimientos=request.POST['padecimientos'],lesiones=request.POST['lesiones'],celular=request.POST['celular'])
                
                # enviar_correo({
                #     'asunto':'Kala Yoga - Registro exitoso',
                #     'template': 'registro_exitoso.html',
                #     'template_ctx': {
                #         'usuario': nombre + ' ' + apellido
                #     },
                #     'correo_usuario': email
                # })

            user.save()
            userInfo.save()
            return redirect('bienvenida')
                
        

    return render(request,'client/signup.html')
   
# Vista de Clases

def verClases(request):
    tipo_clase = Tipo_Clase.objects.all()

    # #Cortar la descripcion
    # for a in tipo_clase:
    #     descripcion = a.descripcion
    #     descripcion = descripcion.split(' ')
    #     descripcion_corta = ''
    #     x = 0
    #     while(x < 20):
    #         descripcion_corta = descripcion_corta + descripcion[x]+' '
    #         x += 1
    #     a.descripcion = descripcion_corta
    # #/.Cortar la descripcion
        
    return render(request, 'client/clases.html', {'tipoClases': tipo_clase})

def siguientesFechas(d, letradia):
    fechasSiguientes = []

    if letradia == 'L':
        weekday = 0
    elif letradia == 'K':
        weekday = 1
    elif letradia == 'M':
        weekday = 2
    elif letradia == 'J':
        weekday = 3
    elif letradia == 'V':
        weekday = 4
    else :
        weekday = 5

    diaHoy = d.weekday()
    if diaHoy == weekday:
       fechasSiguientes.append(datetime.date.today())
        
    days_ahead = weekday - d.weekday()
    if days_ahead <= 0: # Target day already happened this week
        days_ahead += 7
    fechasSiguientes.append(d + datetime.timedelta(days_ahead))
    return fechasSiguientes


def verClaseDetalle(request, pk):
    currentuser = request.user
    tipo_clase = Tipo_Clase.objects.get(id=pk)

    fechas = siguientesFechas(datetime.date.today(),tipo_clase.dia)

    for a in fechas:
        try:
            eventoxreservar = Eventos.objects.get(fecha=a,tipoClase_id=tipo_clase)
            print('evento ya existe')

            try:
                Reservaciones.objects.get(user_id=currentuser.id,evento_id=eventoxreservar.id)
                fechas = []
                fechas.append('msj')
                return render(request,'client/clase_detalle.html', {'tipo_clase': tipo_clase, 'fechas': fechas})   

            except Reservaciones.DoesNotExist:
                return render(request,'client/clase_detalle.html', {'tipo_clase': tipo_clase, 'fechas': fechas})   
            

        except Eventos.DoesNotExist:
            return render(request,'client/clase_detalle.html', {'tipo_clase': tipo_clase, 'fechas': fechas})   

@login_required
def crearReserva(request,id_clase):

    if request.method == 'POST':
        formData = request.POST

    currentuser = request.user
    fechaa = formData['fecha']
    

    try:
        # Caso de que evento ya existe

        eventoxreservar = Eventos.objects.get(fecha=fechaa,tipoClase_id=id_clase)

        # Crear reserva
        reserva = Reservaciones.objects.create(
            evento_id = eventoxreservar.id,
            user_id = currentuser.id
        )    
        reserva.save()
        messages.info(request, 'Reserva realizada con exito')


    except Eventos.DoesNotExist:
        evento = Eventos.objects.create(
            estado=True,
            fecha=fechaa,
            tipoClase_id=id_clase
        )
        evento.save()
        print('evento creado')

        eventoxreservar = Eventos.objects.latest('id')
        print('id de evento x reservar: ',eventoxreservar.id)
        
        reserva = Reservaciones.objects.create(
            evento_id = eventoxreservar.id,
            user_id = currentuser.id
        )    
        reserva.save()
        messages.info(request, 'Reserva realizada con exito')

    # Crear Notificación
    # currentuser = request.user
    # eventoxcancelar = Eventos.objects.get(id = reserva.evento.id)

    # usuarioNombre = currentuser.first_name + ' ' + currentuser.last_name
    # clase = Tipo_Clase.objects.get(id=eventoxcancelar.tipoClase_id)
    # claseNombre = clase.nombre
    # claseDia = clase.dia
    # notificacion = Notificaciones.objects.create(
    #     visto = 0,
    #     mensaje = usuarioNombre + ' creó una nueva reservación para la próxima clase de ' + claseNombre +' ('+claseDia+')'
    # )
    # notificacion.save()
    
    return redirect('clases')

@login_required
def cuenta(request):

    if request.method == 'POST':

        userInfoObj = UsuarioInfo.objects.get(usuario_id=request.user.id)
        userObj = User.objects.get(id=request.user.id)

        #se reemplazan los valores actuales con los que el usuario escribió
        userObj.first_name = request.POST['nombre']
        userObj.last_name = request.POST['apellido']
        userObj.email = request.POST['correo']
        
        userInfoObj.celular = request.POST['celular']
        userInfoObj.edad = request.POST['edad']
        userInfoObj.genero = request.POST['genero']
        userInfoObj.lesiones = request.POST['lesiones']
        userInfoObj.padecimientos = request.POST['padecimientos']
        #se guardan los valores
        User.save(userObj)
        UsuarioInfo.save(userInfoObj)
        #se redirige a la pagina de clientes
        return redirect('cuenta')


    currentuser = request.user

    currentuser = request.user
    usuarioInfo = UsuarioInfo.objects.get(usuario=currentuser.id)
    currentdate = date.today().isoformat()
    currentdate1abajo = date.today()+timedelta(days=-1)

    ultimo_mes = (date.today()-timedelta(days=30)).isoformat()
    siguiente_mes = (date.today()+timedelta(days=30)).isoformat()  

    reservasActivas = Reservaciones.objects.select_related('evento').filter(user_id = currentuser.id,evento__fecha__range=[currentdate,siguiente_mes])

    historialReservas = Reservaciones.objects.select_related('evento').filter(user_id = currentuser.id,evento__fecha__range=[ultimo_mes,currentdate1abajo])

    pedidos_realizados = Orden.objects.filter(cliente=currentuser)
    
    for pedido in pedidos_realizados:
        total_pedido = 0
        detalle_pedido = Orden_detalle.objects.filter(orden=pedido)
        for detalle in detalle_pedido:
            total_pedido = total_pedido + (detalle.cantidad * detalle.precio_unidad)
        pedido.total_orden = total_pedido
        pedido.save()

    pedidos_realizados = Orden.objects.filter(cliente=currentuser)

    return render(request, 'client/cuenta.html', { 'usuario': usuarioInfo, 'reservasActivas' : reservasActivas, 'historialReservas':historialReservas, 'pedidos_realizados': pedidos_realizados })

def cancelarReserva(request,id_reserva):

    reserva = Reservaciones.objects.get(id=id_reserva)
    reserva.delete()

    
    # # Crear Notificación
    # currentuser = request.user
    # eventoxcancelar = Eventos.objects.get(id = reserva.evento.id)

    # usuarioNombre = currentuser.first_name + ' ' + currentuser.last_name
    # clase = Tipo_Clase.objects.get(id=eventoxcancelar.tipoClase_id)
    # claseNombre = clase.nombre
    # claseHora = clase.horario_inicio
    # notificacion = Notificaciones.objects.create(
    #     visto = 0,
    #     mensaje = usuarioNombre + ' canceló una reservación de la clase ' + claseNombre + ' del día: ' + clase.dia
    # )

    # notificacion.save()

    return redirect('cuenta')

def contacto(request):
    formData = request.POST

    if request.method == 'POST':
        contacto = Contactos.objects.create(
            nombre = formData['nombre'],
            correo = formData['email'],
            celular = formData['celular'],
            mensaje = formData['mensaje']
        )
        contacto.save()
        enviar_correo({
            'asunto':'Formulario de contacto',
            'template':'formulario_contacto.html',
            'template_ctx':{
                'cliente':formData['nombre'],
                'email':formData['email'],
                'celular':formData['celular'],
                'mensaje':formData['mensaje'],
                },
            'correo_usuario': 'info@whivetech.com'
            })
        messages.info(request,'Correo enviado exitosamente') 

    return render(request, 'client/contacto.html')

def tienda(request):
        productos = Producto.objects.all()
        return render(request, 'client/tienda.html', {'productos': productos, 'action': 'list'})

def ayuda(request):
        return render(request, 'client/ayuda.html')        

def producto(request, id):
    # Si el id es cambiado desde el URL guarda en la orden un producto diferente¿?
    producto_solicitado = Producto.objects.get(id = id)

    # Cuando un usuario hace click en "Añadir al carrito"
    if request.method=='POST':
        if request.user.is_authenticated:
            currentuser = request.user
            formData = request.POST
            
            cantidad_solicitada = int(formData['cantidad'])
            # Checkear si currentuser tiene una orden en estado EN_PROGRESO
            try:
                orden_en_progreso = Orden.objects.get(cliente=currentuser, estado='EN_PROGRESO')
                try:
                    detalle_actualizar = Orden_detalle.objects.get(orden=orden_en_progreso, producto=producto_solicitado)
                    detalle_actualizar.cantidad = detalle_actualizar.cantidad + cantidad_solicitada
                    detalle_actualizar.save()
                except:
                    nuevo_detalle = Orden_detalle.objects.create(
                        orden=orden_en_progreso,
                        producto=producto_solicitado,
                        cantidad=formData['cantidad'],
                        precio_unidad=producto_solicitado.precio
                    )
                    nuevo_detalle.save()

                # Reducir stock
                valor_stock_actualizado = producto_solicitado.cantidad - int(formData['cantidad'])
                Producto.objects.filter(id=producto_solicitado.id).update(cantidad=valor_stock_actualizado)
    
            except:
                orden_nueva = Orden.objects.create(
                    cliente= currentuser
                )
                orden_nueva.save()

                detalle_nueva_orden = Orden_detalle.objects.create(
                    orden=orden_nueva,
                    producto=producto_solicitado,
                    cantidad=formData['cantidad'],
                    precio_unidad=producto_solicitado.precio
                )
                detalle_nueva_orden.save()
                #Reducir stock
                valor_stock_actualizado = producto_solicitado.cantidad - int(formData['cantidad'])
    
                Producto.objects.filter(id=producto_solicitado.id).update(cantidad=valor_stock_actualizado)

            getOrdenCount(request, currentuser)
            return redirect('carrito')

        else:
            return redirect('carrito')

    return render(request, 'client/tienda_item.html', { 'productos': producto_solicitado})

def carrito(request):
    if request.user.is_authenticated:
        try:

            orden_en_progreso = Orden.objects.get(cliente=request.user, estado='EN_PROGRESO')
            detalle_orden = Orden_detalle.objects.filter(orden=orden_en_progreso).select_related('producto')
            
            total_orden = 0

            for detalle in detalle_orden:
                total_orden = total_orden + detalle.total
  
            return render(request,'client/carrito.html', {
                'productos_carrito': detalle_orden,
                'total_orden': total_orden,
                'orden_id': orden_en_progreso.id
            })

        except:
    
            return render(request,'client/carrito.html')    
    else:
        return render(request,'client/carrito.html')

@login_required
def confirmarPedido(request, orden_id):
    if request.method == 'POST':
        total_orden = 0
        orden_a_confirmar = Orden.objects.get(id=orden_id)
        
        detalle_orden = Orden_detalle.objects.filter(orden=orden_a_confirmar)

        for detalle in detalle_orden:
            total_orden = total_orden + detalle.total

        orden_a_confirmar.estado = "CONFIRMADO"
        orden_a_confirmar.fecha_confirmado = datetime.datetime.now()
        orden_a_confirmar.total_orden = total_orden
        orden_a_confirmar.save()

        request.session['total_items_orden'] = 0

        return render(request, 'client/carrito.html', { 'orden_confirmada': True })

def nosotros(request):
    return render(request, 'client/nosotros.html')

def masajes(request):
    return render(request, 'client/masajes.html')
def clasesyoga(request):
    return render(request, 'client/clasesyoga.html')
    
def retiros(request):
    return render(request, 'client/retiros.html')
def talleres(request):
    return render(request, 'client/talleres.html')

def error_404_view(request, exception):
    return render (request, 'client/404.html')
    
def getOrdenCount(request, user):
    try:
        orden_en_progreso = Orden.objects.get(cliente=user, estado="EN_PROGRESO")
        total_items_orden = Orden_detalle.objects.filter(orden=orden_en_progreso)
        total = 0
        for item in total_items_orden:
            total = item.cantidad + total
        request.session['total_items_orden'] = total
    except:
        request.session['total_items_orden'] = 0

@login_required
def getDetalleOrden(request, orden_id):
        try:
            detalles = []
            detalle_orden = Orden_detalle.objects.filter(orden=orden_id)
            for detalle in detalle_orden:
                producto_info = Producto.objects.get(id=detalle.producto_id)
                detalles.append({
                  'imagen':producto_info.imagen.url,
                  'nombre': producto_info.nombre,
                  'cantidad': detalle.cantidad,
                  'total_detalle': detalle.total  
                })
            detalle_orden = json.dumps(detalles)
            return HttpResponse(detalle_orden, content_type='application/json')
        except:
            return HttpResponse({}, content_type='application/json')

def delete_detalle_orden(request, id_producto):
    if request.method == 'POST':
        
        orden_en_progreso = Orden.objects.get(cliente=request.user, estado="EN_PROGRESO")
        detalle = Orden_detalle.objects.get(orden=orden_en_progreso, producto=id_producto)
        
        # Restablecer stock del producto
        producto_restablecer_stock = Producto.objects.get(id=detalle.producto.id)
        producto_restablecer_stock.cantidad = producto_restablecer_stock.cantidad + detalle.cantidad
        producto_restablecer_stock.save()
        detalle.delete()

        total_items_orden_actualizado = Orden_detalle.objects.filter(orden=orden_en_progreso).count()
        request.session['total_items_orden'] = total_items_orden_actualizado
        return redirect('carrito')

def change_password(request):
    print(request.POST)
    print(request.user)
    return render(request, 'client/cuenta/cuenta-reset-complete.html', { 'changed': True })

def password_reset_view_custom(request):
    # Cargar formulario de Olvide mi contraseña
    # POST -> verificar que el correo que se ingreso existe
    pass

def password_reset_done_view_custom(request):
    pass

def password_reset_confirm_view_custom(request, uidb64, token):
    try:
            # urlsafe_base64_decode() decodes to bytestring
        uid = urlsafe_b64decode(uidb64).decode()
        user = User._default_manager.get(pk=uid)
        
        print('Checking if token is valid')
        validToken = default_token_generator.check_token(user, token)
        
        if validToken:
            if request.method == 'POST':
                formData = request.POST
            
                newPassword = formData['new_password1']
                user.set_password(newPassword)
                user.save()
                return redirect('password_reset_complete')
        else:
            print('Token is invalid')
            return render(request, 'registration/password_reset_confirm.html', { 'validlink':  False })

    except (
        TypeError,
        ValueError,
        OverflowError,
        User.DoesNotExist,
        ):
        return render(request, 'registration/password_reset_confirm.html', { 'validlink':  False })
    return render(request, 'registration/password_reset_confirm.html')

def password_reset_complete_view_custom(request):
    pass